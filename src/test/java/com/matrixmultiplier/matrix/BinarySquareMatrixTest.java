package com.matrixmultiplier.matrix;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class BinarySquareMatrixTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForWrongDimensionedSquareMatrixInput() {
        int[][] input = {{1}, {1, 0}};
        new BinarySquareMatrix(input);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForNotBinarySquareMatrixInput() {
        int[][] input = {{5, 0}, {1, 4}};
        new BinarySquareMatrix(input);
    }

    @Test
    public void shouldReturnValidMatrixDimension2() {
        int[][] input = {{1, 0}, {1, 1}};
        assertEquals(input.length, new BinarySquareMatrix(input).getDimension());
    }

    @Test
    public void shouldReturnValidMatrixDimension3() {
        int[][] input = {{1, 0, 1}, {1, 1, 0}, {1, 1, 1}};
        assertEquals(input.length, new BinarySquareMatrix(input).getDimension());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfElementIndexHigherThanBound() {
        int[][] input = {{1, 0}, {1, 0}};
        new BinarySquareMatrix(input).getElement(0, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfElementIndexLowerThanBound() {
        int[][] input = {{1, 0}, {1, 0}};
        new BinarySquareMatrix(input).getElement(-1, 1);
    }

    @Test
    public void shouldReturnValidElement() {
        int[][] input = {{1, 0}, {1, 0}};
        BinarySquareMatrix matrix = new BinarySquareMatrix(input);

        assertEquals(input[0][1], matrix.getElement(0, 1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfRowIndexLowerThanBound() {
        int[][] input = {{1, 0}, {1, 0}};
        new BinarySquareMatrix(input).getRow(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfRowIndexHigherThanBound() {
        int[][] input = {{1, 0}, {1, 0}};
        new BinarySquareMatrix(input).getRow(2);
    }

    @Test
    public void shouldReturnValidRow() {
        int[][] input = {{1, 0}, {1, 0}};
        BinarySquareMatrix matrix = new BinarySquareMatrix(input);

        assertArrayEquals(new int[]{1, 0}, matrix.getRow(0));
    }
}
