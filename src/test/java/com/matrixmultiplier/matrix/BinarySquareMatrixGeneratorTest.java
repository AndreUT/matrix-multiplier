package com.matrixmultiplier.matrix;

import com.matrixmultiplier.MatrixMultiplierApplication;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BinarySquareMatrixGeneratorTest {

    private static final int DIMENSION_LOWER_LIMIT = 1;

    private BinarySquareMatrixGenerator generator = MatrixMultiplierApplication.factory()
            .matrixGenerator();

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForDimensionLessThanLowerLimit() {
        int dimension = DIMENSION_LOWER_LIMIT - 1;
        generator.generate(dimension);
    }

    @Test
    public void shouldGenerateValidBinarySquareMatrix() {
        BinarySquareMatrix result = generator.generate(DIMENSION_LOWER_LIMIT);

        assertEquals(DIMENSION_LOWER_LIMIT, result.getDimension());
    }

}