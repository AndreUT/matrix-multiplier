package com.matrixmultiplier.comparison;

import com.matrixmultiplier.matrix.BinarySquareMatrix;
import com.matrixmultiplier.matrix.BinarySquareMatrixGenerator;
import com.matrixmultiplier.multiplier.strategy.parallel.MatrixDimensionBasedThreadsCountResolver;
import com.matrixmultiplier.multiplier.strategy.parallel.ParallelMatrixMultiplicationStrategy;
import com.matrixmultiplier.multiplier.strategy.serial.SerialMatrixMultiplicationStrategy;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class MatrixMultipliersComparatorTest {

    private class TestMatrixGenerator extends BinarySquareMatrixGenerator {

        private int numberOfCall = 0;

        @Override
        public BinarySquareMatrix generate(int dimension) {
            if (dimension == MOCKED_MATRICES_DIMENSION) {
                BinarySquareMatrix result = mockGenerationResults.get(numberOfCall % mockGenerationResults.size());
                numberOfCall++;
                return result;
            }
            return super.generate(dimension);
        }
    }

    private static final int ARBITRARY_MATRIX_DIMENSION = 100;
    private static final int MOCKED_MATRICES_DIMENSION = 2;

    private List<BinarySquareMatrix> mockGenerationResults = Arrays.asList(
            new BinarySquareMatrix(new int[][]{{1, 0}, {1, 1}}),
            new BinarySquareMatrix(new int[][]{{1, 1}, {1, 1}}));

    private MatrixMultipliersComparator comparator = new MatrixMultipliersComparator(
            new SerialMatrixMultiplicationStrategy(),
            new ParallelMatrixMultiplicationStrategy(new MatrixDimensionBasedThreadsCountResolver()),
            new TestMatrixGenerator());

    @Test
    public void shouldReturnConcreteResultsForMockedDimension() {
        MultipliersComparisonResult result = comparator.compareMultipliers(MOCKED_MATRICES_DIMENSION);

        assertEquals(mockGenerationResults.get(0), result.getOperand1());
        assertEquals(MOCKED_MATRICES_DIMENSION, result.getOperand1().getDimension());

        assertEquals(mockGenerationResults.get(1), result.getOperand2());
        assertEquals(MOCKED_MATRICES_DIMENSION, result.getOperand2().getDimension());

        ConcreteMultiplierWorkResults parallelMultiplierResults = result.getParallelMultiplierResults();
        assertEquals(MOCKED_MATRICES_DIMENSION, parallelMultiplierResults.getResult().getDimension());

        ConcreteMultiplierWorkResults serialMultiplierResults = result.getSerialMultiplierResults();
        assertEquals(MOCKED_MATRICES_DIMENSION, serialMultiplierResults.getResult().getDimension());

        assertEquals(parallelMultiplierResults.getResult(), serialMultiplierResults.getResult());
    }

    @Test
    public void shouldReturnValidComparisonResultsForRestDimensions() {
        MultipliersComparisonResult result = comparator.compareMultipliers(ARBITRARY_MATRIX_DIMENSION);

        assertNotNull(result.getOperand1());
        assertNotNull(result.getOperand2());

        assertEquals(ARBITRARY_MATRIX_DIMENSION, result.getOperand1().getDimension());
        assertEquals(ARBITRARY_MATRIX_DIMENSION, result.getOperand2().getDimension());

        ConcreteMultiplierWorkResults parallelMultiplierResults = result.getParallelMultiplierResults();
        assertEquals(ARBITRARY_MATRIX_DIMENSION, parallelMultiplierResults.getResult().getDimension());

        ConcreteMultiplierWorkResults serialMultiplierResults = result.getSerialMultiplierResults();
        assertEquals(ARBITRARY_MATRIX_DIMENSION, serialMultiplierResults.getResult().getDimension());

        assertEquals(parallelMultiplierResults.getResult(), serialMultiplierResults.getResult());
    }

}