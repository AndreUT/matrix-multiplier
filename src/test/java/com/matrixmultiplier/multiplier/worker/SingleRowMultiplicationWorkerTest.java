package com.matrixmultiplier.multiplier.worker;

import com.matrixmultiplier.matrix.BinarySquareMatrix;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SingleRowMultiplicationWorkerTest {

    @Test
    public void shouldCorrectlyMultiplyTwoDimensionalRowByMatrix() {
        int[] row = new int[]{1, 1};
        int[][] m2 = new int[][]{{1, 1}, {1, 0}};

        int[] result = new SingleRowMultiplicationWorker(row, new BinarySquareMatrix(m2)).call();

        assertArrayEquals(new int[]{0, 1}, result);
    }

    @Test
    public void shouldCorrectlyMultiplyTwoDimensionalRowByMatrix2() {
        int[] row = new int[]{1, 0};
        int[][] m2 = new int[][]{{1, 1}, {1, 0}};

        int[] result = new SingleRowMultiplicationWorker(row, new BinarySquareMatrix(m2)).call();

        assertArrayEquals(new int[]{1, 1}, result);
    }

    @Test
    public void shouldCorrectlyMultiplyThreeDimensionedRowByMatrix() {
        int[] row = new int[]{1, 1, 1};
        int[][] m2 = new int[][]{{1, 0, 1}, {1, 1, 1}, {0, 1, 0}};

        int[] result = new SingleRowMultiplicationWorker(row, new BinarySquareMatrix(m2)).call();

        assertArrayEquals(new int[]{0, 0, 0}, result);
    }

    @Test
    public void shouldCorrectlyMultiplyThreeDimensionedRowByMatrix2() {
        int[] row = new int[]{1, 1, 0};
        int[][] m2 = new int[][]{{1, 0, 1}, {1, 1, 1}, {0, 1, 0}};

        int[] result = new SingleRowMultiplicationWorker(row, new BinarySquareMatrix(m2)).call();

        assertArrayEquals(new int[]{0, 1, 0}, result);
    }

}