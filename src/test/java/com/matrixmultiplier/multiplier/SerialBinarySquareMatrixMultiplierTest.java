package com.matrixmultiplier.multiplier;

import com.matrixmultiplier.matrix.BinarySquareMatrix;
import com.matrixmultiplier.multiplier.strategy.serial.SerialMatrixMultiplicationStrategy;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SerialBinarySquareMatrixMultiplierTest {

    private SerialMatrixMultiplicationStrategy serialStrategy =
            new SerialMatrixMultiplicationStrategy();
    private ValidatingBinarySquareMatricesMultiplicator multiplier =
            new ValidatingBinarySquareMatricesMultiplicator(serialStrategy);

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForMatrixWithDifferentDimensions() {
        int[][] m1 = new int[][]{{0, 0}, {1, 1}};
        int[][] m2 = new int[][]{{0, 0, 0}, {1, 1, 1}, {0, 1, 1}};

        multiplier.multiply(new BinarySquareMatrix(m1), new BinarySquareMatrix(m2));
    }

    @Test
    public void shouldCorrectlyMultiplyTwoDimensionedMatrices() {
        int[][] m1 = new int[][]{{0, 1}, {1, 1}};
        int[][] m2 = new int[][]{{1, 1}, {1, 0}};

        BinarySquareMatrix result = multiplier.multiply(new BinarySquareMatrix(m1), new BinarySquareMatrix(m2));

        assertEquals(new BinarySquareMatrix(new int[][]{{1, 0}, {0, 1}}), result);
    }

    @Test
    public void shouldCorrectlyMultiplyTwoDimensionedMatrices2() {
        int[][] m1 = new int[][]{{1, 1}, {1, 1}};
        int[][] m2 = new int[][]{{1, 1}, {1, 0}};

        BinarySquareMatrix result = multiplier.multiply(new BinarySquareMatrix(m1), new BinarySquareMatrix(m2));

        assertEquals(new BinarySquareMatrix(new int[][]{{0, 1}, {0, 1}}), result);
    }

    @Test
    public void shouldCorrectlyMultiplyThreeDimensionedMatrices2() {
        int[][] m1 = new int[][]{{1, 1, 1}, {1, 1, 1}, {0, 0, 1}};
        int[][] m2 = new int[][]{{1, 1, 0}, {0, 1, 1}, {1, 1, 0}};

        BinarySquareMatrix result = multiplier.multiply(new BinarySquareMatrix(m1), new BinarySquareMatrix(m2));

        assertEquals(new BinarySquareMatrix(new int[][]{{0, 1, 1}, {0, 1, 1}, {1, 1, 0}}), result);
    }

}