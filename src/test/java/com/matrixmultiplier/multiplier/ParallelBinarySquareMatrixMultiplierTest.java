package com.matrixmultiplier.multiplier;

import com.matrixmultiplier.MatrixMultiplierApplication;
import com.matrixmultiplier.matrix.BinarySquareMatrix;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParallelBinarySquareMatrixMultiplierTest {

    private BinarySquareMatricesMultiplicator multiplier = MatrixMultiplierApplication.factory()
            .parallelValidatingMultiplier();

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForMatrixWithDifferentDimensions() {
        int[][] m1 = new int[][]{{0, 0}, {1, 1}};
        int[][] m2 = new int[][]{{0, 0, 0}, {1, 1, 1}, {0, 1, 1}};

        multiplier.multiply(new BinarySquareMatrix(m1), new BinarySquareMatrix(m2));
    }

    @Test
    public void shouldCorrectlyMultiplyTwoDimensionedMatrices() {
        int[][] m1 = new int[][]{{0, 1}, {1, 1}};
        int[][] m2 = new int[][]{{1, 1}, {1, 0}};

        BinarySquareMatrix result = multiplier.multiply(new BinarySquareMatrix(m1), new BinarySquareMatrix(m2));

        assertEquals(new BinarySquareMatrix(new int[][]{{1, 0}, {0, 1}}), result);
    }

    @Test
    public void shouldCorrectlyMultiplyTwoDimensionedMatrices2() {
        int[][] m1 = new int[][]{{1, 1}, {1, 1}};
        int[][] m2 = new int[][]{{1, 1}, {1, 0}};

        BinarySquareMatrix result = multiplier.multiply(new BinarySquareMatrix(m1), new BinarySquareMatrix(m2));

        assertEquals(new BinarySquareMatrix(new int[][]{{0, 1}, {0, 1}}), result);
    }

    @Test
    public void shouldCorrectlyMultiplyThreeDimensionedMatrices2() {
        int[][] m1 = new int[][]{{1, 1, 1}, {1, 1, 1}, {0, 0, 1}};
        int[][] m2 = new int[][]{{1, 1, 0}, {0, 1, 1}, {1, 1, 0}};

        BinarySquareMatrix result = multiplier.multiply(new BinarySquareMatrix(m1), new BinarySquareMatrix(m2));

        assertEquals(new BinarySquareMatrix(new int[][]{{0, 1, 1}, {0, 1, 1}, {1, 1, 0}}), result);
    }

    @Test
    public void shouldCorrectlyMultiplyThreeDimensionedMatrices3() {
        int[][] m1 = new int[][]{{1, 1, 1}, {1, 0, 1}, {0, 0, 0}};
        int[][] m2 = new int[][]{{1, 1, 0}, {0, 0, 1}, {1, 1, 0}};

        BinarySquareMatrix result = multiplier.multiply(new BinarySquareMatrix(m1), new BinarySquareMatrix(m2));

        assertEquals(new BinarySquareMatrix(new int[][]{{0, 0, 1}, {0, 0, 0}, {0, 0, 0}}), result);
    }

    @Test
    public void shouldCorrectlyMultiplyFourDimensionedMatrices() {
        int[][] m1 = new int[][]{{1, 1, 1, 0}, {1, 0, 1, 1}, {0, 0, 0, 1}, {0, 1, 0, 0}};
        int[][] m2 = new int[][]{{0, 0, 1, 0}, {1, 1, 1, 1}, {0, 1, 0, 1}, {0, 0, 1, 0}};

        BinarySquareMatrix result = multiplier.multiply(new BinarySquareMatrix(m1), new BinarySquareMatrix(m2));

        assertEquals(new BinarySquareMatrix(new int[][]{{1, 0, 0, 0}, {0, 1, 0, 1}, {0, 0, 1, 0}, {1, 1, 1, 1}}),
                result);
    }

}