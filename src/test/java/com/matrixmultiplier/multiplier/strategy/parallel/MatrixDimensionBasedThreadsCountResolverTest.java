package com.matrixmultiplier.multiplier.strategy.parallel;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MatrixDimensionBasedThreadsCountResolverTest {

    private static final int DEFAULT_THREADS_COUNT = 64;

    private Map<Integer, Integer> matrixDimensionToThreadsCount = new HashMap<Integer, Integer>()
    {{
        put(25, 1);
        put(250, 8);
        put(500, 16);
        put(1000, 32);
        put(10000, 64);
    }};

    private MatrixDimensionBasedThreadsCountResolver resolver =
            new MatrixDimensionBasedThreadsCountResolver();

    @Test
    public void shouldReturnValidThreadsCountForDimension24() {
        assertEquals(matrixDimensionToThreadsCount.get(25),
                Integer.valueOf(resolver.resolveThreadsFromMatrixDimension(24)));
    }

    @Test
    public void shouldReturnValidThreadsCountForDimension249() {
        assertEquals(matrixDimensionToThreadsCount.get(250),
                Integer.valueOf(resolver.resolveThreadsFromMatrixDimension(249)));
    }

    @Test
    public void shouldReturnValidThreadsCountForDimension499() {
        assertEquals(matrixDimensionToThreadsCount.get(500),
                Integer.valueOf(resolver.resolveThreadsFromMatrixDimension(499)));
    }

    @Test
    public void shouldReturnValidThreadsCountForDimension999() {
        assertEquals(matrixDimensionToThreadsCount.get(1000),
                Integer.valueOf(resolver.resolveThreadsFromMatrixDimension(999)));
    }

    @Test
    public void shouldReturnValidThreadsCountForDimension9999() {
        assertEquals(matrixDimensionToThreadsCount.get(10000),
                Integer.valueOf(resolver.resolveThreadsFromMatrixDimension(9999)));
    }

    @Test
    public void shouldReturnValidThreadsCountForDimension1000000() {
        assertEquals(Integer.valueOf(DEFAULT_THREADS_COUNT),
                Integer.valueOf(resolver.resolveThreadsFromMatrixDimension(1000000)));
    }

}