package com.matrixmultiplier;

import java.util.Scanner;

public class MatrixMultiplierApplication {

    private static final int MATRIX_SIZE_LOWER_LIMIT = 1;
    private static final int MATRIX_SIZE_UPPER_LIMIT = 10000;

    private static BeanFactory factory = new BeanFactory();

    public static BeanFactory factory() {
        return factory;
    }

    public static void main(String[] args) {
        new MatrixMultiplierApplication().runApp();
    }

    private void runApp() {
        System.out.println("Enter dimension of matrices you want to multiply...");
        int dimension = convertUserInputToMatrixSize(getUserInput());
        validateMatrixSize(dimension);
        System.out.println(factory().multipliersComparator().compareMultipliers(dimension));
    }

    private void validateMatrixSize(int dimension) {
        if (dimension < MATRIX_SIZE_LOWER_LIMIT || dimension > MATRIX_SIZE_UPPER_LIMIT) {
            throw new IllegalArgumentException("Matrix size should be in range between " + MATRIX_SIZE_LOWER_LIMIT
                    + ", " + MATRIX_SIZE_UPPER_LIMIT);
        }
    }

    private int convertUserInputToMatrixSize(String input) {
        try {
            return Integer.parseInt(input);
        } catch (Exception exception) {
            System.err.println();
            throw new IllegalArgumentException("Invalid matrix size: " + input);
        }
    }

    private String getUserInput() {
        Scanner inScanner = new Scanner(System.in);
        return inScanner.nextLine();
    }

}
