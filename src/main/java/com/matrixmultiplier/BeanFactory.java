package com.matrixmultiplier;

import com.matrixmultiplier.comparison.MatrixMultipliersComparator;
import com.matrixmultiplier.matrix.BinarySquareMatrixGenerator;
import com.matrixmultiplier.multiplier.BinarySquareMatricesMultiplicator;
import com.matrixmultiplier.multiplier.ValidatingBinarySquareMatricesMultiplicator;
import com.matrixmultiplier.multiplier.strategy.parallel.MatrixDimensionBasedThreadsCountResolver;
import com.matrixmultiplier.multiplier.strategy.parallel.ParallelMatrixMultiplicationStrategy;
import com.matrixmultiplier.multiplier.strategy.serial.SerialMatrixMultiplicationStrategy;

public class BeanFactory {

    public BinarySquareMatricesMultiplicator parallelValidatingMultiplier() {
        return new ValidatingBinarySquareMatricesMultiplicator(parallelMultiplicationStrategy());
    }

    public BinarySquareMatricesMultiplicator serialValidatingMultiplier() {
        return new ValidatingBinarySquareMatricesMultiplicator(serialMultiplicationStrategy());
    }

    public BinarySquareMatricesMultiplicator parallelMultiplicationStrategy() {
        return new ParallelMatrixMultiplicationStrategy(threadsCountResolver());
    }

    public MatrixDimensionBasedThreadsCountResolver threadsCountResolver() {
        return new MatrixDimensionBasedThreadsCountResolver();
    }

    public BinarySquareMatricesMultiplicator serialMultiplicationStrategy() {
        return new SerialMatrixMultiplicationStrategy();
    }

    public BinarySquareMatrixGenerator matrixGenerator() {
        return new BinarySquareMatrixGenerator();
    }

    public MatrixMultipliersComparator multipliersComparator() {
        return new MatrixMultipliersComparator(serialValidatingMultiplier(),
                parallelValidatingMultiplier(),
                matrixGenerator());
    }

}
