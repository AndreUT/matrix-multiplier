package com.matrixmultiplier.multiplier.strategy.serial;

import com.matrixmultiplier.matrix.BinarySquareMatrix;
import com.matrixmultiplier.multiplier.BinarySquareMatricesMultiplicator;

import java.util.Arrays;

import static java.util.stream.IntStream.range;

public class SerialMatrixMultiplicationStrategy implements BinarySquareMatricesMultiplicator {
    @Override
    public BinarySquareMatrix multiply(BinarySquareMatrix matrix1, BinarySquareMatrix matrix2) {
        int dimension = matrix1.getDimension();
        int[][] resultMatrixArray = Arrays.stream(matrix1.getMatrix())
                .map(row -> range(0, dimension)
                        .map(rowIndex -> range(0, dimension)
                                .map(columnIndex -> row[columnIndex] & matrix2.getElement(columnIndex, rowIndex))
                                .reduce(0, (c1, c2) -> c1 ^ c2)).toArray())
                .toArray(int[][]::new);
        return new BinarySquareMatrix(resultMatrixArray);
    }
}
