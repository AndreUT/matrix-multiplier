package com.matrixmultiplier.multiplier.strategy.parallel;

import java.util.*;
import java.util.stream.Collectors;

public class MatrixDimensionBasedThreadsCountResolver {

    private static final int DEFAULT_THREADS_COUNT = 64;

    private final Map<Integer, Integer> matrixDimensionToThreadsCount = new HashMap<Integer, Integer>()
    {{
        put(25, 1);
        put(250, 8);
        put(500, 16);
        put(1000, 32);
        put(10000, 64);
    }};

    public int resolveThreadsFromMatrixDimension(int matrixDimension) {
        List<Integer> collect = matrixDimensionToThreadsCount.keySet()
                .stream()
                .filter(dimension -> dimension >= matrixDimension)
                .collect(Collectors.toList());

        return collect.stream()
                .min(Integer::compareTo)
                .map(matrixDimensionToThreadsCount::get)
                .orElse(DEFAULT_THREADS_COUNT);
    }
}
