package com.matrixmultiplier.multiplier.strategy.parallel;

import com.matrixmultiplier.matrix.BinarySquareMatrix;
import com.matrixmultiplier.multiplier.BinarySquareMatricesMultiplicator;
import com.matrixmultiplier.multiplier.worker.SingleRowMultiplicationWorker;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ParallelMatrixMultiplicationStrategy implements BinarySquareMatricesMultiplicator {

    private final MatrixDimensionBasedThreadsCountResolver threadsCountResolver;

    public ParallelMatrixMultiplicationStrategy(MatrixDimensionBasedThreadsCountResolver threadsCountResolver) {
        this.threadsCountResolver = threadsCountResolver;
    }

    @Override
    public BinarySquareMatrix multiply(BinarySquareMatrix matrix1, BinarySquareMatrix matrix2) {
        int dimension = matrix1.getDimension();

        ExecutorService executorService = Executors.newFixedThreadPool(threadsCountResolver
                .resolveThreadsFromMatrixDimension(dimension));
        List<SingleRowMultiplicationWorker> workers = IntStream.range(0, dimension)
                .mapToObj(matrix1::getRow)
                .map(row -> new SingleRowMultiplicationWorker(row, matrix2))
                .collect(Collectors.toList());
        int[][] resultMatrixArray = workers.stream().map(executorService::submit)
                .collect(Collectors.toList()).stream().map(this::getFutureResult).toArray(int[][]::new);

        executorService.shutdown();
        return new BinarySquareMatrix(resultMatrixArray);
    }

    private Serializable getFutureResult(Future<int[]> rowFuture) {
        try {
            return rowFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException("Error occurred during parallel  execution of matrices multiplication", e);
        }
    }
}
