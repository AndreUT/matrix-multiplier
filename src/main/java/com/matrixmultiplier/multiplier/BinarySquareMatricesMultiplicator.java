package com.matrixmultiplier.multiplier;

import com.matrixmultiplier.matrix.BinarySquareMatrix;

public interface BinarySquareMatricesMultiplicator {

    BinarySquareMatrix multiply(BinarySquareMatrix matrix1, BinarySquareMatrix matrix2);

}
