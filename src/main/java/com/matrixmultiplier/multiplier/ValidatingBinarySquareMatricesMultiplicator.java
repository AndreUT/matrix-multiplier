package com.matrixmultiplier.multiplier;

import com.matrixmultiplier.matrix.BinarySquareMatrix;

public class ValidatingBinarySquareMatricesMultiplicator implements BinarySquareMatricesMultiplicator {

    private final BinarySquareMatricesMultiplicator strategy;

    public ValidatingBinarySquareMatricesMultiplicator(BinarySquareMatricesMultiplicator strategy) {
        this.strategy = strategy;
    }

    @Override
    public BinarySquareMatrix multiply(BinarySquareMatrix matrix1,
                                       BinarySquareMatrix matrix2) {
        validateDimensionsForMultiplication(matrix1, matrix2);
        return strategy.multiply(matrix1, matrix2);
    }

    private void validateDimensionsForMultiplication(BinarySquareMatrix matrix1, BinarySquareMatrix matrix2) {
        if (matrix1.getDimension() != matrix2.getDimension()) {
            throw new IllegalArgumentException("Matrices should have same dimension");
        }
    }
}
