package com.matrixmultiplier.multiplier.worker;

import com.matrixmultiplier.matrix.BinarySquareMatrix;

import java.util.concurrent.Callable;

import static java.util.stream.IntStream.range;

public class SingleRowMultiplicationWorker implements Callable<int[]> {

    private int[] row;
    private BinarySquareMatrix secondOperandMatrix;

    public SingleRowMultiplicationWorker(int[] row, BinarySquareMatrix secondOperandMatrix) {
        this.row = row;
        this.secondOperandMatrix = secondOperandMatrix;
    }

    @Override
    public int[] call() {
        int dimension = secondOperandMatrix.getDimension();
        int[][] matrix = secondOperandMatrix.getMatrix();
        return range(0, dimension)
                .map(i -> range(0, dimension)
                        .map(j -> row[j] & matrix[j][i])
                        .reduce(0, (c1, c2) -> c1 ^ c2))
                .toArray();
    }

}