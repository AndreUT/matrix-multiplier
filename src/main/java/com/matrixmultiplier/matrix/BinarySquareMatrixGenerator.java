package com.matrixmultiplier.matrix;

import static java.util.stream.IntStream.range;

public class BinarySquareMatrixGenerator {

    private static final int DIMENSION_LOWER_LIMIT = 1;

    public BinarySquareMatrix generate(int dimension) {
        validateDimension(dimension);
        int[][] randomMatrix = range(0, dimension).mapToObj(rowIndex -> range(0, dimension)
                .map(columnIndex -> getRandomBinaryInt()).toArray()).toArray(int[][]::new);
        return new BinarySquareMatrix(randomMatrix);
    }

    private int getRandomBinaryInt() {
        return (int) (Math.random() * 2);
    }

    private void validateDimension(int dimension) {
        if (dimension < DIMENSION_LOWER_LIMIT) {
            throw new IllegalArgumentException("Matrix dimension should be from more than 0");
        }
    }

}
