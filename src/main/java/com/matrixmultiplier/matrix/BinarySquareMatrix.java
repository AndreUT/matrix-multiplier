package com.matrixmultiplier.matrix;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static java.util.Arrays.stream;

public class BinarySquareMatrix {

    private static final List<Integer> allowedCellValues = Arrays.asList(0, 1);

    private int[][] matrix;
    private int dimension;

    public BinarySquareMatrix(int[][] matrix) {
        validateInputDimension(matrix);
        validateInputCells(matrix);
        this.matrix = matrix;
        this.dimension = matrix.length;
    }

    private void validateInputDimension(int[][] matrix) {
        final int rowsCount = matrix.length;
        boolean isMatrixSquare = stream(matrix).allMatch(row -> row.length == rowsCount);
        if (!isMatrixSquare) {
            throw new IllegalArgumentException("Provided matrix is not a square matrix");
        }
    }

    private void validateInputCells(int[][] matrix) {
        boolean isMatrixHasOnlyBinaryValues = stream(matrix)
                .allMatch(row -> stream(row)
                        .allMatch(allowedCellValues::contains));
        if (!isMatrixHasOnlyBinaryValues) {
            throw new IllegalArgumentException("Provided matrix is not a binary matrix");
        }
    }

    public int getElement(int i, int j) {
        if (Stream.of(i, j).anyMatch(index -> index < 0 || index >= dimension)) {
            throw new IllegalArgumentException("Invalid indices");
        }
        return matrix[i][j];
    }

    public int[] getRow(int i) {
        if (i < 0 || i >= dimension) {
            throw new IllegalArgumentException("Row index out of bound");
        }
        return matrix[i];
    }

    public int[][] getMatrix() {
        return matrix.clone();
    }

    public int getDimension() {
        return dimension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BinarySquareMatrix that = (BinarySquareMatrix) o;
        return dimension == that.dimension &&
                Arrays.deepEquals(matrix, that.matrix);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(dimension);
        result = 31 * result + Arrays.hashCode(matrix);
        return result;
    }

    @Override
    public String toString() {
        return "BinarySquareMatrix{" +
                "matrix=" + Arrays.deepToString(matrix) +
                '}';
    }
}
