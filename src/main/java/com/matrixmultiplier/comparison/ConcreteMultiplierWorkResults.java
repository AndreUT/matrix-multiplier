package com.matrixmultiplier.comparison;

import com.matrixmultiplier.matrix.BinarySquareMatrix;

import java.util.Objects;

public class ConcreteMultiplierWorkResults {
    private BinarySquareMatrix result;
    private long multiplicationTimeInMillis;

    public ConcreteMultiplierWorkResults(BinarySquareMatrix result, long multiplicationTime) {
        this.result = result;
        this.multiplicationTimeInMillis = multiplicationTime;
    }

    public BinarySquareMatrix getResult() {
        return result;
    }

    public long getMultiplicationTimeInMillis() {
        return multiplicationTimeInMillis;
    }

    @Override
    public String toString() {
        return "ConcreteMultiplierWorkResults{" +
                "\nmultiplicationTimeInMillis=" + multiplicationTimeInMillis +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConcreteMultiplierWorkResults that = (ConcreteMultiplierWorkResults) o;
        return multiplicationTimeInMillis == that.multiplicationTimeInMillis &&
                Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(result, multiplicationTimeInMillis);
    }
}
