package com.matrixmultiplier.comparison;

import com.matrixmultiplier.matrix.BinarySquareMatrix;
import com.matrixmultiplier.matrix.BinarySquareMatrixGenerator;
import com.matrixmultiplier.multiplier.BinarySquareMatricesMultiplicator;

public class MatrixMultipliersComparator {

    private final BinarySquareMatricesMultiplicator serialMultiplier;
    private final BinarySquareMatricesMultiplicator parallelMultiplier;
    private final BinarySquareMatrixGenerator matrixGenerator;

    public MatrixMultipliersComparator(BinarySquareMatricesMultiplicator serialMultiplier,
                                       BinarySquareMatricesMultiplicator parallelMultiplier,
                                       BinarySquareMatrixGenerator matrixGenerator) {
        this.serialMultiplier = serialMultiplier;
        this.parallelMultiplier = parallelMultiplier;
        this.matrixGenerator = matrixGenerator;
    }

    public MultipliersComparisonResult compareMultipliers(int matrixDimension) {
        BinarySquareMatrix matrix1 = matrixGenerator.generate(matrixDimension);
        BinarySquareMatrix matrix2 = matrixGenerator.generate(matrixDimension);
        ConcreteMultiplierWorkResults serialMultiplicationResults = getMultiplicationResults(serialMultiplier,
                matrix1, matrix2);
        ConcreteMultiplierWorkResults parallelMultiplicationResults = getMultiplicationResults(parallelMultiplier,
                matrix1, matrix2);
        return new MultipliersComparisonResult(matrix1, matrix2, parallelMultiplicationResults,
                serialMultiplicationResults);
    }

    private ConcreteMultiplierWorkResults getMultiplicationResults(BinarySquareMatricesMultiplicator multiplier,
                                                                   BinarySquareMatrix matrix1,
                                                                   BinarySquareMatrix matrix2) {
        long startMillis = System.currentTimeMillis();
        BinarySquareMatrix result = multiplier.multiply(matrix1, matrix2);
        long finishMillis = System.currentTimeMillis();
        return new ConcreteMultiplierWorkResults(result, finishMillis - startMillis);
    }

}
