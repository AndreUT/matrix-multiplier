package com.matrixmultiplier.comparison;

import com.matrixmultiplier.matrix.BinarySquareMatrix;

public class MultipliersComparisonResult {

    private BinarySquareMatrix operand1;
    private BinarySquareMatrix operand2;
    private ConcreteMultiplierWorkResults parallelMultiplierResults;
    private ConcreteMultiplierWorkResults serialMultiplierResults;

    public MultipliersComparisonResult(BinarySquareMatrix operand1,
                                       BinarySquareMatrix operand2,
                                       ConcreteMultiplierWorkResults parallelMultiplierResults,
                                       ConcreteMultiplierWorkResults serialMultiplierResults) {
        this.operand1 = operand1;
        this.operand2 = operand2;
        this.parallelMultiplierResults = parallelMultiplierResults;
        this.serialMultiplierResults = serialMultiplierResults;
    }

    public BinarySquareMatrix getOperand1() {
        return operand1;
    }

    public BinarySquareMatrix getOperand2() {
        return operand2;
    }

    public ConcreteMultiplierWorkResults getParallelMultiplierResults() {
        return parallelMultiplierResults;
    }

    public ConcreteMultiplierWorkResults getSerialMultiplierResults() {
        return serialMultiplierResults;
    }

    @Override
    public String toString() {
        return "MultipliersComparisonResult{" +
                ",\nparallelMultiplierResults=" + parallelMultiplierResults +
                ",\nserialMultiplierResults=" + serialMultiplierResults +
                '}';
    }
}
